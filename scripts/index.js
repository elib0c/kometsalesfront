const pathUrl = 'http://localhost:8080/';

axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    return Promise.reject(error.response);
});

new Vue({
    el: '#app',
    vuetify: new Vuetify({
    icons: {
            iconfont: 'fa'
        },
    }),
    mounted() {
        this.getSellers();
    },
    data () {
        return {
            snackbar: {
                show: false,
                message: '',
                color: 'red'
            },
            loading: false,
            drawer: false,
            showDialog: false,
            gridView: 'table', // or cards
            sellers: [],
            selectedSeller: [],
            sellerSelectedSales: [],
            file: null
        }
    },
    computed: {
        salesCommission () {
            return this.sellerSelectedSales.reduce((a, b) => a + (b['amount'] * this.selectedSeller.commissionPercentage / 100 || 0), 0);
        }
    },
    methods: {
        getSellers () {
            this.loading = true;
            this.drawer = false;
            axios.get(pathUrl + 'seller/').then(
                response => {
                    this.loading = false;
                    if (response.status === 200) {
                        this.sellers = response.data;
                    }
                }
            ).catch(
                errors => {
                    this.snackbar.message = "Internal Server Error";
                    this.snackbar.color = 'red';
                    this.snackbar.show = true;
                    this.loading = false;
                }
            )
        },
        getSales (seller_id) {
            this.loading = true;
            axios.get(pathUrl + 'sale/' + seller_id).then(
                response => {
                    this.loading = false;
                    if (response.status === 200)  {
                        this.sellerSelectedSales = response.data;
                        this.showDialog = true;
                    }
                }
            ).catch(
                errors => {
                    this.snackbar.message = "Internal Server Error";
                    this.snackbar.color = 'red';
                    this.snackbar.show = true;
                    this.loading = false;
                }
            )
        },
        selectSeller (seller) {
            this.selectedSeller = seller;
            this.sellerSelectedSales = [];
            this.getSales(seller.id);
        },
        selectFile () {
            // Fix: Reiniciar file input
            this.$refs.fileupload.type = 'text';
            this.$refs.fileupload.type = 'file';
            this.file = null;

            document.getElementById('file').click();
            this.drawer = false;
        },
        upload (event) {
            this.file = event.target.files[0];
            if (this.file) {
                this.loading = true;
                let formData = new FormData();
                formData.append('file', this.file);
                axios.post(pathUrl + 'sale/upload', formData, {headers: {'Content-Type': 'multipart/form-data'}}).then(
                    response => {
                        this.loading = false;
                        this.file = null;
                        if (response.status === 200)  {
                            this.snackbar.message = "¡Ventas cargadas correctamente!";
                            this.snackbar.color = 'green';
                        } else {
                            this.snackbar.message = response.data;
                            this.snackbar.color = 'red';
                        }

                        this.snackbar.show = true;
                    }
                ).catch(
                    errors => {
                        this.file = null;
                        this.snackbar.message = '<strong>' + errors.data.error + ':' + '</strong>' + errors.data.message;
                        this.snackbar.color = 'red';
                        this.snackbar.show = true;
                        this.loading = false;
                    }
                )
            }
        }
    }
})